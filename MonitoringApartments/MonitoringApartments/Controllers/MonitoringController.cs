using BusinessLayer;
using DataLayer.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MonitoringApartments.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/logs")]
    public class MonitoringController : ApiController
    {
        private IMonitoringBusiness monitorBusiness;

        public MonitoringController(IMonitoringBusiness monitorBusiness)
        {
            this.monitorBusiness = monitorBusiness;
        }

        [Route("getall")]
        public List<Monitoring> GetAllLogs()
        {
            return this.monitorBusiness.GetAllLog();
        }

        [Route("insert")]
        [HttpPost]
        public bool InsertLog([FromBody]Monitoring m)
        {
            return this.monitorBusiness.InsertLog(m);
        }
    }
}